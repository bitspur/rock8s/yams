include $(MKPM)/mkpm
include $(MKPM)/dotenv

export NETWORK_IP ?= $(shell ip addr show $(NETWORK_BRIDGE) | grep -E 'inet ' | sed 's| *inet *||g' | cut -d' ' -f1 | cut -d'/' -f1 | head -n1)
export S3_ENDPOINT := http://$(NETWORK_IP):7480
TERRAFORM ?= terraform

.PHONY: format
format:
	@$(TERRAFORM) fmt --recursive

.PHONY: init
init:
	@. $(HOME)/yaps/scripts/proxmox-auth.sh && \
		. $(CURDIR)/variables.sh && \
		$(TERRAFORM) init -migrate-state -backend-config="path=$(APP_DIR)/mailcow.tfstate" && \
		sh $(HOME)/yaps/scripts/proxmox-deauth.sh

.PHONY: apply
apply: init
	@. $(HOME)/yaps/scripts/proxmox-auth.sh && \
		. $(CURDIR)/variables.sh && \
		$(TERRAFORM) apply -auto-approve && \
		sh $(HOME)/yaps/scripts/proxmox-deauth.sh

.PHONY: install
install: radosgw
	@export S3_ACCESS_KEY="$$(sudo radosgw-admin user info --uid=s3 | jq -r '.keys[0].access_key')" && \
	export S3_SECRET_KEY="$$(sudo radosgw-admin user info --uid=s3 | jq -r '.keys[0].secret_key')" && \
	$(call make) apply

.PHONY: destroy
destroy: init
	@. $(HOME)/yaps/scripts/proxmox-auth.sh && \
		. $(CURDIR)/variables.sh && \
		$(TERRAFORM) destroy && \
		sh $(HOME)/yaps/scripts/proxmox-deauth.sh
